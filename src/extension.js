const { St, Clutter } = imports.gi;
const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Mainloop = imports.mainloop;

class WCExtension extends PanelMenu.Button {
    constructor() {
        super(0.0, 'Word Count Extension');

        log('WCExtension: Initializing');

        this.label = new St.Label({
            text: "Loading...",
            y_align: Clutter.ActorAlign.CENTER,
            style_class: 'wc-label'
        });
        this.add_child(this.label);

        this._updateLabel();
        this._timeout = Mainloop.timeout_add_seconds(10, this._updateLabel.bind(this));
    }

    _updateLabel() {
        log('WCExtension: Updating label');
        try {
            let filePath = GLib.get_home_dir() + "/.histfile";
            log(`WCExtension: File path - ${filePath}`);
            let file = Gio.file_new_for_path(filePath);
            if (file.query_exists(null)) {
                log('WCExtension: File exists');
                let [success, contents] = file.load_contents(null);
                log(`WCExtension: File load success - ${success}`);
                if (success) {
                    let text = contents.toString();
                    log(`WCExtension: File contents length - ${text.length}`);
                    let wordCount = this._countWords(text);
                    log(`WCExtension: Word count - ${wordCount}`);
                    this.label.set_text(`Words: ${wordCount}`);
                } else {
                    logError('WCExtension: Error reading file');
                    this.label.set_text("Error reading file");
                }
            } else {
                logError('WCExtension: File does not exist');
                this.label.set_text("File does not exist");
            }
        } catch (e) {
            logError(e, 'WCExtension: Failed to read file');
            this.label.set_text("Error");
        }
        return true; // Continue timeout
    }

    _countWords(text) {
        log('WCExtension: Counting words');
        if (!text) {
            log('WCExtension: No text provided');
            return 0;
        }
        let wordArray = text.trim().split(/\s+/).filter(word => word.length > 0);
        log(`WCExtension: Word array length - ${wordArray.length}`);
        return wordArray.length;
    }

    destroy() {
        log('WCExtension: Destroying');
        if (this._timeout) {
            Mainloop.source_remove(this._timeout);
            this._timeout = null;
        }
        super.destroy();
    }
}

function init() {
    log('WCExtension: Initializing extension');
}

let _indicator;

function enable() {
    log('WCExtension: Enabling extension');
    _indicator = new WCExtension();
    Main.panel.addToStatusArea('wc-indicator', _indicator);
}

function disable() {
    log('WCExtension: Disabling extension');
    if (_indicator) {
        _indicator.destroy();
        _indicator = null;
    }
}

