**Commando**

Commando is a GNOME Shell extension designed to keep track of the number of commands you've entered into your shell session. Whether you're a casual user or a power user, Commando helps you stay informed about your command-line activity.

### Features:
- Tracks the number of commands executed in your shell session.
- Provides a visual indicator to show the current command count.
- Simple and unobtrusive design.
- Easy to install and configure.

### License:
Commando is licensed under the GNU Affero General Public License v3.0 (AGPLv3). See the [LICENSE](LICENSE) file for more details.


**Enjoy tracking your command-line activity with Commando!** 🚀