## How does Bash store command history:
Bash stores its command history in the main memory in a file called as .bash_history.

The three related environment variables you need to be aware of are HISTFILE, HISTFILESIZE, and HISTSIZE.

- HISTFILE—/home/<username>/.bash_history
- HISTFILESIZE—1000
- HISTSIZE—1000  

The HISTFILE variable holds the name and location of your Bash history file. 

HISTFILESIZE is how many commands can be stored in the .bash_history file. 

HISTSIZE is the number of cached commands. Once you reach 1000 commands, the oldest commands will be discarded as new ones are saved.

## Problem 1:

The problem with Bash history is that it's not written to the .bash_history file until you log off. This makes it impossible to use the history command for scripting.

Before being written, it is stored in RAM and can be accessed by the command:
```sh
history
```
[-- Source --](https://www.redhat.com/sysadmin/history-command)
## Solution:

To work around this feature, use the write option for the history command. To find the number of commands written, we need to write it into .bash_history evertheytime we check the number of commands we have written. 
```sh
history -w
```
I tried going with something like this:

```sh
#!/bin/bash

history -w
# Path to the Bash history file
history_file="$HOME/.bash_history"

# Count the number of lines (commands) in the history file
num_commands=$(wc -l < "$history_file")

echo "Total number of commands in the history file: $num_commands"
```

But this is causing the top entries of the current .bash_history file to dissapear and keeping the command count at around 1000


Creating a GNOME Shell extension that performs the wc command on the file ~/.histfile and displays the output in the top bar involves several steps. Below is a guide on how to achieve this.
Prerequisites

    Basic Knowledge of JavaScript: GNOME Shell extensions are written in JavaScript.

    Development Tools: Install GNOME Shell development tools and necessary dependencies. You can typically install these using your package manager. For example, on Ubuntu-based systems:

    bash

    sudo apt-get install gnome-shell-extension-tool gnome-shell-extensions

    GNOME Shell Extensions Website: Refer to the GNOME Shell Extensions website for more detailed documentation.

Steps to Create the Extension

    Create Directory Structure:

    bash

mkdir -p ~/.local/share/gnome-shell/extensions/wc@yourdomain.com
cd ~/.local/share/gnome-shell/extensions/wc@yourdomain.com

Create Metadata File (metadata.json):

json

{
    "uuid": "wc@yourdomain.com",
    "name": "Word Count Extension",
    "description": "Displays wc output of ~/.histfile in the top bar",
    "shell-version": ["40", "41", "42"],
    "version": 1
}

Create the Extension Code (extension.js):

javascript

const St = imports.gi.St;
const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const Lang = imports.lang;
const GLib = imports.gi.GLib;

const WCExtension = class WCExtension extends PanelMenu.Button {
    _init() {
        super._init(0.0, 'Word Count Extension', false);

        this.label = new St.Label({
            text: "Loading...",
            y_align: Clutter.ActorAlign.CENTER
        });
        this.add_actor(this.label);

        this._updateLabel();
        this._timeout = Mainloop.timeout_add_seconds(10, Lang.bind(this, this._updateLabel));
    }

    _updateLabel() {
        try {
            let [ok, out, err, exit] = GLib.spawn_command_line_sync('wc ~/.histfile');
            if (ok) {
                this.label.set_text(out.toString().trim());
            } else {
                this.label.set_text("Error");
            }
        } catch (e) {
            logError(e, 'Failed to execute wc command');
        }
        return true; // Continue timeout
    }

    destroy() {
        if (this._timeout) {
            Mainloop.source_remove(this._timeout);
            this._timeout = null;
        }
        super.destroy();
    }
};

function init() {}

function enable() {
    this._indicator = new WCExtension();
    Main.panel.addToStatusArea('wc-indicator', this._indicator);
}

function disable() {
    this._indicator.destroy();
    this._indicator = null;
}

Install the Extension:

    Navigate to ~/.local/share/gnome-shell/extensions/ and make sure your extension folder (wc@yourdomain.com) is there.
    Reload GNOME Shell (Press Alt+F2, type r, and press Enter).

Enable the Extension:

    Use GNOME Tweaks or run the following command:

    bash

    gnome-extensions enable wc@yourdomain.com

Test and Debug:

    Check if the extension is loaded and if the word count is displayed in the top bar.
    For debugging, you can view the logs by running:

    bash

        journalctl /usr/bin/gnome-shell -f

Explanation

    metadata.json: Contains metadata about the extension including its UUID, name, description, supported shell versions, and version.
    extension.js: The main script that defines the extension’s behavior. It uses GNOME Shell's APIs to create a button in the top bar that displays the word count of ~/.histfile.
    _updateLabel Function: Executes the wc command and updates the label text every 10 seconds.
    GLib.spawn_command_line_sync
